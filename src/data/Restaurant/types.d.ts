export type IRestaurant = {
  id: string
  name: string
  info: {
    coverPhotoUrl: string
  }
}
