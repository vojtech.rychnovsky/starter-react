import { action } from 'typesafe-actions'
import {
  LOAD_RESTAURANTS_REQUEST,
  LOAD_RESTAURANTS_SUCCESS,
  LOAD_RESTAURANTS_FAILURE,
} from './constants'
import { IRestaurant } from './types'

export const loadRestaurantsRequest = () => action(LOAD_RESTAURANTS_REQUEST)

export const loadRestaurantsSuccess = (restaurants: IRestaurant[]) =>
  action(LOAD_RESTAURANTS_SUCCESS, restaurants)

export const loadRestaurantsFailure = () => action(LOAD_RESTAURANTS_FAILURE)
