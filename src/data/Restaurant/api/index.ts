import api from '../../apiClient'
import { IRestaurant } from '../types'

interface IGetRestaurantResponse {
  data: IRestaurant[]
}

/* QUERIES */
export const requestGetRestaurants = async () => {
  return (await api<IGetRestaurantResponse>('/subpath')).data
}
