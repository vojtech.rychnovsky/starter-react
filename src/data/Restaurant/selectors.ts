import { IReduxState } from '../store'

export const selectRestaurants = (state: IReduxState) =>
  state.restaurant.restaurants
