import { Reducer } from 'redux'
import { ActionType } from 'typesafe-actions'
import { IRestaurant } from './types'
import { LOAD_RESTAURANTS_SUCCESS } from './constants'

export type IRestaurantActions = ActionType<typeof import('./actions')>

export type IRestaurantState = ImmutableObject<{
  restaurants: IRestaurant[]
}>

const initState: IRestaurantState = {
  restaurants: [],
}

const restaurantReducer: Reducer<IRestaurantState, IRestaurantActions> = (
  state = initState,
  action
) => {
  switch (action.type) {
    case LOAD_RESTAURANTS_SUCCESS:
      return { restaurants: action.payload }
    default:
      return state
  }
}

export default restaurantReducer
