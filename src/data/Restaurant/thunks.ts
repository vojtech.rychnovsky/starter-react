import { TAction } from '../store'
import {
  loadRestaurantsRequest,
  loadRestaurantsSuccess,
  loadRestaurantsFailure,
} from './actions'
import * as restaurantApi from './api'

export const handleGetRestaurants: TAction<void> = () => async dispatch => {
  dispatch(loadRestaurantsRequest())

  try {
    const result = await restaurantApi.requestGetRestaurants()
    dispatch(loadRestaurantsSuccess(result))
  } catch (e) {
    dispatch(loadRestaurantsFailure())
  }
}
