import { Reducer } from 'redux'
import { ActionType } from 'typesafe-actions'

export type IAppActions = ActionType<typeof import('./actions')>

export type IAppState = ImmutableObject<{}>

const initState: IAppState = {}

const appReducer: Reducer<IAppState, IAppActions> = (
  state = initState,
  action
) => {
  switch (action.type) {
    default:
      return state
  }
}

export default appReducer
